<!-- start page title -->
<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('add_student_results'); ?></h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<div class="row">
    <div class="col-12">

<div class="row justify-content-center">
    <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
              <div class="col-lg-12">
                <h4 class="mb-3 header-title"><?php echo get_phrase('result_form'); ?></h4>

                <form class="required-form" action="<?php echo site_url('admin/result_add/result_add'); ?>" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="user_id"><?php echo get_phrase('user'); ?><span class="required">*</span> </label>
                        <select class="form-control select2" data-toggle="select2" name="user_id" id="user_id" required>
                            <option value=""><?php echo get_phrase('select_a_user'); ?></option>
                            <?php $user_list = $this->user_model->get_user()->result_array();
                                foreach ($user_list as $user):?>
                                <option value="<?php echo $user['id'] ?>"><?php echo $user['first_name'].' '.$user['last_name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="course_id"><?php echo get_phrase('course'); ?><span class="required">*</span> </label>
                        <select class="form-control select2" data-toggle="select2" name="course_id" id="course_id" required>
                            <option value=""><?php echo get_phrase('select_a_course'); ?></option>
                            <?php $course_list = $this->crud_model->get_courses()->result_array();
                                foreach ($course_list as $course):
                                if ($course['status'] != 'active')
                                    continue;?>
                                <option value="<?php echo $course['id'] ?>"><?php echo $course['title']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="event_id"><?php echo get_phrase('event'); ?><span class="required">*</span> </label>
                        <select class="form-control select2" data-toggle="select2" name="event_id" id="event_id" required>
                            <option value=""><?php echo get_phrase('select_a_event'); ?></option>
                            <?php $event_list = $this->crud_model->get_event()->result_array();
                                foreach ($event_list as $event):
                                if ($course['status'] != 'active')
                                    continue;?>
                                <option value="<?php echo $event['id'] ?>"><?php echo $event['title']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="marks"><?php echo get_phrase('marks'); ?><span class="required">*</span></label>
                        <input type="integer" class="form-control" id="marks" name = "marks" required>
                    </div>
                    <div class="form-group">
                        <label for="practicals"><?php echo get_phrase('practicals'); ?><span class="required">*</span></label>
                        <select  class="form-control select2" data-toggle="select2" name="practicals" id="practicals" required>
                        <option value=""><?php echo get_phrase('Grade_practical'); ?></option>
                            <option value ="Pass"><?php echo get_phrase('pass'); ?></option>
                            <option value ="N/R"><?php echo get_phrase('Need remediation (N/R)'); ?></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="resit_marks"><?php echo get_phrase('resit_marks'); ?></label>
                        <input type="text" class="form-control" id="resit_marks" name = "resit_marks">
                    </div>
                    <div class="form-group">
                        <label for="resit_practicals"><?php echo get_phrase('resit_practicals'); ?></label>
                         <select  class="form-control select2" data-toggle="select2" name="resit_practicals" id="resit_practical">
                        <option value=""><?php echo get_phrase('Grade_practical'); ?></option>
                            <option value ="Pass"><?php echo get_phrase('pass'); ?></option>
                            <option value ="N/R"><?php echo get_phrase('Need remediation (N/R)'); ?></option>
                        </select>
                    </div>
                    <button type="button" class="btn btn-primary" onclick="checkRequiredFields()"><?php echo get_phrase('add_student_result'); ?></button>
                </form>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
