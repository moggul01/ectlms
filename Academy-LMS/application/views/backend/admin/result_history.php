<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo $page_title; ?>
                <a href = "<?php echo site_url('admin/result_add'); ?>" class="btn btn-outline-primary btn-rounded alignToTitle"><i class="mdi mdi-plus"></i><?php echo get_phrase('add_results'); ?></a>
            </h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
              <h4 class="mb-3 header-title"><?php echo get_phrase('result_list'); ?></h4>
              <div class="table-responsive-sm mt-4">
                <table id="basic-datatable" class="table table-striped table-centered mb-0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo get_phrase('user'); ?></th>
                      <th><?php echo get_phrase('course'); ?></th>
                      <th><?php echo get_phrase('event'); ?></th>
                      <th><?php echo get_phrase('marks'); ?></th>
                      <th><?php echo get_phrase('practicals'); ?></th>
                      <th><?php echo get_phrase('resit_marks'); ?></th>
                      <th><?php echo get_phrase('resit_practicals'); ?></th>
                      <th><?php echo get_phrase('actions'); ?></th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                       foreach ($results->result_array() as $key => $result): ?>
                          <tr>
                              <td><?php echo $key+1; ?></td>
                            
                              <td><?php echo $result['user_id']; ?></td>
                              <td><?php echo $result['course_id']; ?></td>
                              <td><?php echo $result['event_id']; ?></td>
                              <td><?php echo $result['marks']; ?></td>
                              <td><?php echo $result['practicals']; ?></td>
                              <td><?php echo $result['resit_marks']; ?></td>
                              <td><?php echo $result['resit_practicals']; ?></td>
                            
                              <td>
                                  <div class="dropright dropright">
                                    <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="mdi mdi-dots-vertical"></i>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="<?php echo site_url('admin/user_form/edit_user_form/'.$user['id']) ?>"><?php echo get_phrase('edit'); ?></a></li>
                                        <li><a class="dropdown-item" href="#" onclick="confirm_modal('<?php echo site_url('admin/result_add/delete/'.$user['id']); ?>');"><?php echo get_phrase('delete'); ?></a></li>
                                    </ul>
                                </div>
                              </td>
                          </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
