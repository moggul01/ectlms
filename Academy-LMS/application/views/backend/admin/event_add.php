<!-- start page title -->
<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('add_new_event'); ?></h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row justify-content-center">
    <div class="col-xl-7">
        <div class="card">
            <div class="card-body">
              <div class="col-lg-12">
                <h4 class="mb-3 header-title"><?php echo get_phrase('event_add_form'); ?></h4>

                <form class="required-form" action="<?php echo site_url('admin/event_add/event_add'); ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="code"><?php echo get_phrase('event_code'); ?></label>
                        <input type="text" class="form-control" id="code" name = "code" value="<?php echo substr(md5(rand(0, 1000000)), 0, 10); ?>" readonly>
                    </div>

                    <div class="form-group">
                        <label for="name"><?php echo get_phrase('event_title'); ?><span class="required">*</span></label>
                        <input type="text" class="form-control" id="title" name = "title" required>
                    </div>

                    <div class="form-group">
                        <label for="course_id"><?php echo get_phrase('course'); ?><span class="required">*</span> </label>
                        <select class="form-control select2" data-toggle="select2" name="course_id" id="course_id" required>
                            <option value=""><?php echo get_phrase('select_a_course'); ?></option>
                            <?php $course_list = $this->crud_model->get_courses()->result_array();
                                foreach ($course_list as $course):
                                if ($course['status'] != 'active')
                                    continue;?>
                                <option value="<?php echo $course['id'] ?>"><?php echo $course['title']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name"><?php echo get_phrase('instructor_name'); ?><span class="required">*</span></label>
                        <input type="text" class="form-control" id="instructor_id" name = "instructor_id" required>
                    </div>
                    <div class="form-group">
                        <label for="place"><?php echo get_phrase('place'); ?><span class="required">*</span></label>
                        <input type="text" class="form-control" id="place" name = "place" required>
                    </div>
                    <div class="form-group">
                        <label for="place"><?php echo get_phrase('event_date'); ?><span class="required">*</span></label>
                        <input type="date" class="form-control" id="event_date" name = "event_date" required>
                    </div>
                          <div class="form-group">
                        <label for="status"><?php echo get_phrase('status'); ?><span class="required">*</span> </label>
                        <select class="form-control select2" data-toggle="select2" name="status" id="status" required>
                            <option value=""><?php echo get_phrase('select_a_status'); ?></option>
    
                                <option value="upcoming"><?php echo get_phrase('Upcoming'); ?></option>
                                <option value="previous"><?php echo get_phrase('Previous'); ?></option>
                          
                        </select>
                    </div>
                    <!-- <div class="form-group" id = "thumbnail-picker-area">
                        <label> <?php echo get_phrase('category_thumbnail'); ?> <small>(<?php echo get_phrase('the_image_size_should_be'); ?>: 400 X 255)</small> </label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="category_thumbnail" name="event_thumbnail" accept="image/*" onchange="changeTitleOfImageUploader(this)">
                                <label class="custom-file-label" for="category_thumbnail"><?php echo get_phrase('choose_thumbnail'); ?></label>
                            </div>
                        </div>
                    </div> -->

                    <button type="button" class="btn btn-primary" onclick="checkRequiredFields()"><?php echo get_phrase("add_event"); ?></button>
                </form>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<script type="text/javascript">
    function checkCategoryType(category_type) {
        if (category_type > 0) {
            $('#thumbnail-picker-area').hide();
            $('#icon-picker-area').hide();
        }else {
            $('#thumbnail-picker-area').show();
            $('#icon-picker-area').show();
        }
    }
</script>
